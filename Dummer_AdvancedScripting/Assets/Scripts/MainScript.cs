﻿using System.Collections;
using System.Collections.Generic;
using System.Threading; //uses this to add delays to text
using UnityEngine;
using InventoryManagement; //contains all self defined classes to be used for this project

public class MainScript : MonoBehaviour //This script runs the actual meat of the game, using all other classes to create a basic text experience.
{
    string[] events = {"You found a helmet! You equip it, changing your stats!", "You come across a monster!!! You fight valiantly, taking a few hits, but prevailing.", "The Goblin drops a new chestpiece! You pick it up and equip it, becoming even more powerful.", "Lastly, you come across a chest containing a pair of gauntlets!" };
    Equipment[] equipment = { new Equipment("Helmet", 5, "Helm of Smarts!"), new Equipment("Chestpiece", 7, "Chestpiece of Destruction!"), new Equipment("Gauntlets", 5, "Gauntlets of Strength")};
    int iterator = 0;
    Player player;
    // Start is called before the first frame update
    void Start()
    {
        Stats playerStats = new Stats(100, 100, 5, 5, 5, 5, 5); //defines players starting stats
        player = new Player(playerStats);
        print(player.stats.toString());
        print("Welcome Adventurer! Choose what you'd like to do!");
        print("\n Press w to auto adventure, press i to check your inventory, press s to check your stats, and press q to exit");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            switch (Input.inputString)
            {
                case "w":
                    //print("Started auto adventure");
                    print(events[iterator] + "\n");
                    switch (iterator) {
                        case 0:
                            Inventory.PickUpEquipment(equipment[0]);
                            player.switchEquipment(equipment[0]);
                            print(player.stats.toString());
                            break;
                        case 1:
                            Enemy goblin = new Enemy(new Stats(50, 50, 1, 1, 1, 1, 1));
                            player.attack(goblin, 25); //attacks goblin for 25 damage.
                            player.stats.hp -= player.defend(goblin.attack(25));
                            print("Goblin: ");
                            print(goblin.stats.toString());
                          
                            print("\n Player: ");
                            print(player.stats.toString());
                            player.attack(goblin, 50);
                           
                            print("\n Goblin: ");
                            print(goblin.stats.toString());

                            iterator++;
                            print(events[iterator]);
                            Inventory.PickUpEquipment(equipment[1]);
                            player.switchEquipment(equipment[1]);
                            print(player.stats.toString());
                            break;
                        case 3: //skips to because iterator was incremented in the middle of case 1
                            Inventory.PickUpEquipment(equipment[2]);
                            player.switchEquipment(equipment[2]);
                            print(player.stats.toString());
                            print("Thanks for playing!");
                            break;
                    }
                    iterator++;
                    break;
                case "i": //this opens the Inventory
                    print(Inventory.toString());
                    break;
                case "q":
                    Application.Quit(); //This closes the game when it is properly built outside of engine
                    break;
                case "s": //this prints player stats
                    print(player.stats.toString());
                    break;

            }
        }

    }
}


