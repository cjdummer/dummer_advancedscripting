﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace InventoryManagement
{
    public class Stats //This class defines the Stats data type, which includes hp, mp, str, dex, wis, con, cha
    {
        //Initializes all variables as attributes that can be easily get and set
        public int hp {get; set;}
        public int mp {get; set;}
        public int str {get; set;}
        public int dex {get; set;}
        public int wis {get; set;}
        public int con {get; set;}
        public int cha {get; set;}

        public Stats(int health, int mana, int strength, int dexterity, int wisdom, int constitution, int charisma) //constructor for stats class, initializes stats.
        {
            hp = health;
            mp = mana;
            str = strength;
            dex = dexterity;
            wis = wisdom;
            con = constitution;
            cha = charisma;
        }
        public string toString() //this simplifies the printing of player stats
        {
            return "Health: " + hp + " Mana: " + mp + " Strength: " + str + " Dexterity: " + dex + " Wisdom: " + wis + " Constitution: " + con + " Charisma: " + cha;  
        }

    }

}
