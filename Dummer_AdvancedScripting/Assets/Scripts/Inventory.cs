﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace InventoryManagement 
{
    public class Inventory
    { //This class is used to manage a  backpack of sorts, containing a dictionary of equipment.

        public static List<Equipment> backpack = new List<Equipment>();
        public static void PickUpEquipment(Equipment gear) //This adds equipment to your backpack
        {
            backpack.Add(gear);
        }

       
        static string backpackList;
        public static string toString() //This goes through every item in backpack and prints it.
        {
            backpackList = "";
           foreach(Equipment gear in backpack)
            {
                backpackList += gear.toString() + "\n";
            }
           if(backpackList == "")
            {
                backpackList = "Inventory Empty";
            }
            return backpackList;
            
        }

        public static void Sort() //this sorts the list using the compareTo method found in Equipment
        {
            backpack.Sort();
        }
    
        
    }
}