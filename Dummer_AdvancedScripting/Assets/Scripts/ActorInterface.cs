﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryManagement;

public interface ActorInterface<T> //Interface to be implemented by player and enemy, uses generics for flexibility.
{
    T attack(T damageDealt); //All actors should be able to attack and defend

    T defend(T damageTaken);

}
