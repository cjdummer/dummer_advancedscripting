﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace InventoryManagement
{
    public class Equipment: IComparable<Equipment> //This class defines the equipment data type. Equipment contains an armor type, a stat boost, and a unique name
    {
        public string type;
        public int boost;
        public string name;
        public Equipment(string type1, int boost1, string name1){ 
            type = type1;
            boost = boost1;
            name = name1;
        }


        public string toString() //This makes printing an equipment object easier.
        {
            return "Armor Type: " + type + " Stat Boost: " + boost + " Name: " + name;
        }

        public int CompareTo(Equipment other) //this allows equipment to be sorted in a list.
        {
            if (other.boost < boost)
            {
                return -1;
            }
            if (other.boost == boost)
            {
                return 0;
            }
            if (other.boost > boost)
            {
                return 1;
            }
            return 0;
        }
    
    }
}