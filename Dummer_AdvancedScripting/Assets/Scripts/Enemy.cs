﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryManagement;

public class Enemy : Actor //Inherits from actor 
{
    

    public Enemy(Stats st) : base(st)
    {
        stats = st;
    }

    new public int attack(int damageDealt) //uses the new keyword to hide Actor's declaration
    {
        int crit;
        crit = ExtensionMethods.generateRandom() >= 99 ? 2 : 1; //checks to see if enemy crit
        return damageDealt * crit;
    }

    public override int defend(int damageTaken) //overrides defend to change the method
    {
        damageTaken = damageTaken > 50 ? 50 : 25; //uses ternary operator to shorten if/else
        return damageTaken;
    }
}
