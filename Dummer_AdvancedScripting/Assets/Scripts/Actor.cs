﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryManagement;

public class Actor : ActorInterface<int> //Fills the basic outline of the ActorInterface, is inherited by Player and Enemy as they are both Actors
{
    public Stats stats;
    

    public Actor(Stats st)
    {
        stats = st;
    }

    public virtual int attack(int damageDealt) //both attack and defend are virtual so they can be overridden by children
    {
        return 1;
    }

    public virtual int defend(int damageTaken)
    {
        return 1;
    }
}
