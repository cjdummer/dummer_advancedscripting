﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine;
using System; //uses this to avoid conflicts with UnityEngine.Random which does not do what I want it to.

namespace InventoryManagement
{
    public static class ExtensionMethods //contains methods to be used by other classes.
    {
        public static int generateRandom() //generates and returns a random number between 0 and 100;
        {
            Random random = new Random();
            return random.Next(0, 100);
        }
    }
}