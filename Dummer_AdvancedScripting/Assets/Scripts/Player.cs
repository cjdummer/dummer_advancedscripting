﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InventoryManagement;

public class Player : Actor
{
    public Stats stats;
    public Equipment helm;
    public Equipment chestpiece;
    public Equipment gauntlets;
    public Equipment pants;
    public Equipment boots;
    //public int tempStat;
    public Player(Stats st, Equipment h, Equipment c, Equipment g, Equipment p, Equipment b) : base(st) //allows a player object to be constructed with starting equipment.
    {
        stats = st;
        helm = h;
        chestpiece = c;
        gauntlets = g;
        pants = p;
        boots = b;
    }

    public Player(Stats st) : base(st) //allows a player object to be constructed only with stats.
    {
        stats = st;
    }

    public void switchEquipment(Equipment toBeEquipped) //This allows the player to change what is currently equipped, and handles the changing of stats.
    {
        string temp = toBeEquipped.type;
        switch (temp)
        {
            case "Helmet":
                if (this.helm != null) //checks if there is a helm equipped before trying to unequip.
                {
                    unequip(this.helm, this.helm.boost);
                }
                this.helm = toBeEquipped;
                equip(this.helm, this.helm.boost);
                break;
            case "Chestpiece":
                if (this.chestpiece != null)
                {
                    unequip(this.chestpiece, this.chestpiece.boost);
                }
                this.chestpiece = toBeEquipped;
                equip(this.chestpiece, this.chestpiece.boost);
                break;
            case "Gauntlets":
                if (this.gauntlets != null)
                { 
                    unequip(this.gauntlets, this.gauntlets.boost);
                }
                this.gauntlets = toBeEquipped;
                equip(this.gauntlets, this.gauntlets.boost);
                break;
            case "Pants":
                if (this.pants != null)
                {
                    unequip(this.pants, this.pants.boost);
                }
                this.pants = toBeEquipped;
                equip(this.pants, this.pants.boost);
                break;
            case "Boots":
                if (this.pants != null)
                {
                    unequip(this.boots, this.boots.boost);
                }
                this.boots = toBeEquipped;
                equip(this.boots, this.boots.boost);
                break;
        }
    }

    void unequip(Equipment piece, int change) //This changes stats based on what piece of equipment was taken off
    {
        switch (piece.type)
        {
            case "Helmet": 
                this.stats.wis -= change;
                break;
            case "Chestpiece":
                this.stats.con -= change;
                break;
            case "Gauntlets":
                this.stats.str -= change;
                break;
            case "Pants":
                this.stats.cha -= change;
                break;
            case "Boots":
                this.stats.dex -= change;
                break;
        }

    }

   void equip(Equipment piece, int change) //This changes stats based on what piece of equipment was put on
    {
        switch (piece.type)
        {
            case "Helmet":
                this.stats.wis += change;
                break;
            case "Chestpiece":
                this.stats.con += change;
                break;
            case "Gauntlets":
                this.stats.str += change;
                break;
            case "Pants":
                this.stats.cha += change;
                break;
            case "Boots":
                this.stats.dex += change;
                break;
        }
    }

   
    public void attack(Enemy other, int damage) //These override the methods from actor because player needs more functionality.
    {
        other.stats.hp  -= other.defend(damage); //subtracts defended health value from enemy's health
    }

    public override int defend(int damageTaken)
    {
        return damageTaken / this.stats.con; //player takes less damage with more con
    }
}
